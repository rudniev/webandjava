import java.util.Arrays;

public class CustomArray<T> implements IBaseArray<T> {

	private T _array[];
	private int size = 0;
	private static final int DefaultSize = 10;
	
	public CustomArray() {
		this(DefaultSize);
	}
	
	@SuppressWarnings("unchecked")
	public CustomArray(int lenght) {
		_array = (T[]) new Object[lenght];
	}
	
	@Override
	public void Add(T element) {
		EnsureCapacity();
		_array[size++] = element;
	}

	@Override
	public void Remove(int index) {
		CheckIndex(index);
		int capasity = _array.length;
		
	        if(index != --capasity)
	            System.arraycopy(_array,index +1, _array,index,_array.length - index - 1 );
	}

	@Override
	public Boolean IsExist(T element) {
		for (int i = 0; i < _array.length; i++) {
			if (_array[i] == element) 
				return true;
			}
		return false;
	}
	
	@Override
	public T Get(int index) {
		CheckIndex(index);
		 return _array[index];
	}
	
	private void CheckIndex(int index) {
		if (index >= _array.length || index < 0) 
			 throw new IndexOutOfBoundsException("Must be a non-negative integer and less then array's length"); 
	}
	
	 private void EnsureCapacity()
	    {
	        if(size == _array.length)
	        {
	            int newSize = _array.length * 2;
	            _array = Arrays.copyOf(_array, newSize);
	        }
	    }

	@Override
	public T ExecuteActionWithPredicate(IPredicateFunctionality<T> predicate, int index) {
		CheckIndex(index);
		T value = Get(index);
		return predicate.Action(value);
	}

}
