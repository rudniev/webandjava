@FunctionalInterface
public interface IPredicateFunctionality<T> {
	T Action(T value);
}
