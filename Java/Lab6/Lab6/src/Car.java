
public class Car {
    	@NotNull
	    private String _manufacture;	

	 	private Engine _engine;

	    @IntMinMax(min =1, max = 460)
	    private  int _speed;
	    
	    public Car(String manufacture) {
	        _manufacture = manufacture;
	        _engine = new Engine();
	    }
	    
	    public String getManufacture() {
	        return _manufacture;
	    }

	    public void setManufacture(String manufacture) {
	    	_manufacture = manufacture;
	    }


}
