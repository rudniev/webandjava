public final class TaskHelper {
	
	public static Thread Create(Runnable context) {
		return new Thread(context);
	}

	public static void RunJoinTasks(Thread t1, Thread t2) {		
		try {
			StartTasks(t1,t2);
			JoinTasks(t1,t2);
		}catch (Exception e) {
			e.fillInStackTrace();
		}	
	}
	
	public static void JoinTasks(Thread t1, Thread t2) {
		try {
			t1.join();
			t2.join();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public static void StartTasks(Thread t1, Thread t2) {
		try{
			t1.start();
			t2.start();	
		}catch (Exception e) {
			throw e;
		}	
	}
}
