import java.util.Collection;
import java.util.Random;

final public class Counter implements Runnable {
	
	final private Random rand = new Random();	
	final private Collection<Integer> _syncCollection;
	
	public Counter(Collection<Integer> syncCollection) {
		_syncCollection = syncCollection;
	}
	
	public int GetCountOfNum5() {
		return _syncCollection.size();
	}
	
	public synchronized void run(){
		increment();
	}
	
    public void increment() {
    	for (int i = 0; i < 10000; i++) {
			int randNum = rand.nextInt(10);		
			if(randNum == 5) {
				_syncCollection.add(1);
			}
		}
    }
	
	public void Clean() {
		_syncCollection.clear();
	}
} 
