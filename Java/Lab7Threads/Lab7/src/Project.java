import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Project {
	public static void main(String[] args){
		
		Collection<Integer> syncCollection = Collections.synchronizedCollection(new ArrayList<>());
		Counter counter = new Counter(syncCollection);
		
		Thread t1 = TaskHelper.Create(counter);
		Thread t2 = TaskHelper.Create(counter);
		
		TaskHelper.RunJoinTasks(t1, t2);
		    
		System.out.println(counter.GetCountOfNum5());
	}
}
