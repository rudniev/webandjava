
public interface IBaseArray<T>{
	
	void Add(T element);
	
	void Remove(int index); 
	
	T Get(int index);
	
	Boolean IsExist(T element);
}
