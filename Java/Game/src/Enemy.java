
public class Enemy {
	private int DefaultDmg = 10;
	protected int BonnusDmg;
	
	public Enemy(int bonnusDmg){
		BonnusDmg = bonnusDmg;
	}
	
	public int Attack() {
		return DefaultDmg + BonnusDmg;
	}
}
